@extends('layouts.app')

@section('content')
Получить код подтверждения на почту!<br>
<form action="{{route('send-confirmation-email', $user)}}" method="post">
  {{ csrf_field() }}
<input type="hidden" name="email" value="{{$user->email}}">
<input type="submit" value="Получить код">
</form>

@endsection
