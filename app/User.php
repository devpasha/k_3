<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $guarded=[];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function confirmed(){
      return !! $this->is_confirmed;
    }

    public function getEmailConfirmationToken(){
      $token = Str::random();
      $this->update(['token'=>$token]);
      return $token;
    }

    public function confirm(){
      $this->update([
        'isConfirmed'=> true,
        'token'=>null
      ]);
      return $this;
    }

}
