<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailConfirmation;

class UsersEmailConfirmationController extends Controller
{
    public function request(User $user){
      if($user->confirmed()){
        return redirect()->home(); // что бы подтвержденные пользователи не попадали на страницу получения токена
      }

      return view('request-email-confirmation', compact('user'));
    }

    public function sendEmail(User $user, Request $request){

        // var_dump($request->all());// Что бы увидеть, что приходит то, что необходимо
      $token = $user->getEmailConfirmationToken();
      // print_r($token); // Что бы увидеть, что приходит то, что необходимо
      // die;
      Mail::to($user->email)->send(new EmailConfirmation($user, $token));
    }
    public function confirm(User $user, $token){

      $userToConfirm = User::where('email', $user->email)->where('token', $token)->first();
      if(!$userToConfirm){
        return redirect()->route('request-confirm-email', $user);
      }
      $userToConfirm->confirm();
      return redirect()->home();
    }

}
